package com.example.loginprozor;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.io.IOException;

public class LoginController {
    @FXML
    private TextField username;
    @FXML
    private Button btnLogin;
    @FXML
    private PasswordField password;
    @FXML
    private Label infoLabel;

    private Parent root;
    private Stage stage;
    private Scene scene;

    @FXML
    void onLoginClicked(ActionEvent event) throws IOException{
        // Spremanje teksta iz TextFielda username u varijablu
        String user =username.getText();
        //Provjera username-a i password-a
        if (username.getText().equalsIgnoreCase("ime")
                && password.getText().equalsIgnoreCase("ime")){

            //Dohvaćanje scene
            FXMLLoader loader = new FXMLLoader(getClass().getResource("StartMenu.fxml"));
            //loadanje scene u root varijablu
            root = loader.load();

            // S obzirom na to da, druga scena ima svoj kontroler koji je vezan za UI druge scene
            //potrebno je na određen način pristupiti kontroleru druge scene iz kontrolera prve scene
            // kako bi se postavila vrijednost stringa u labelu druge scene

           // StartMenuController startmenuController = new StartMenuController();
             StartMenuController startMenuController = loader.getController();
           // potrebno je proslijediti korisničko ime setUser metodi

           //  startmenuController.setUser(user);

            //dohvaćanje stagea iz glavne klase
            stage = (Stage)((Node)event.getSource()).getScene().getWindow();
            //postavljanje root node-a na novu scenu
            scene = new Scene(root,600,600);
            // postavljanje nove scene na stage
            stage.setScene(scene);
            stage.show();
        }else{
            // postaviti odgovarajući tekst ako provjera username-a i password-a nije prošla
            infoLabel.setText("Krive login informacije, isprobajte opet");
        }
    }
}


