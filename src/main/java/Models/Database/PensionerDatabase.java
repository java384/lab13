package Models.Database;

import Models.PensioneRepository;
import Models.Pensioner;
import Models.Student;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class PensionerDatabase implements PensioneRepository {
    static Logger log = Logger.getLogger("Models/Database/PensionerDatabase.java");

    List<Pensioner> database = new ArrayList<Pensioner>();


    public PensionerDatabase(){
        FileHandler fileHandler;
        try {
            fileHandler = new FileHandler("PensionerDatabase.log");
            log.addHandler(fileHandler);
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
            log.info("Logger initialised");
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("Database init!");

        database.add(new Pensioner(UUID.randomUUID(), "sdas", "Peasdtković", 30000,40,324));
        database.add(new Pensioner(UUID.randomUUID(), "Perisdca", "Psderić", 38900,22,34));
        database.add(new Pensioner(UUID.randomUUID(), "Narsdko", "Nyarksdo", 45000,35,43));


    }
    @Override
    public Pensioner get(UUID id) {
        for(Pensioner s :database){
            if(s.getUuid().compareTo(id)==0){
                return s;
            }
        }
        return null;
    }

    @Override
    public void add(Pensioner pensioner) {
        database.add(pensioner);

    }

    @Override
    public boolean update(Pensioner pensioner) {
        for (Pensioner s : database){
            if(s.getUuid() == pensioner.getUuid()){
                s.setFirstName(pensioner.getFirstName());
                s.setLastname(pensioner.getLastname());
                s.setAccountBalance(pensioner.getAccountBalance());
                s.setWorkingAge(pensioner.getWorkingAge());
                return true;
            }
        }
        database.add(pensioner);
        return false;
    }

    @Override
    public void remove(Pensioner pensioner) {
        database.remove(pensioner);
    }

    @Override
    public List<Pensioner> getAllPensioners() {
        log.warning("GetAllPensioners have been called!");
        return database;
    }
}
