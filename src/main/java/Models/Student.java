package Models;

import Utilities.GeneralTaxingConstants;

import java.util.Objects;
import java.util.UUID;

public class Student extends Person{

    protected double hourlyRate;
    private double getHourlyRateThreshold = 25;

    public Student(UUID uuid, String firstName, String lastname, double accountBalance, double hourlyRate) {
        super(uuid, firstName, lastname, accountBalance);
        this.hourlyRate=hourlyRate;
    }
    //copy constructor
    public Student(Student original){
        super(UUID.randomUUID(),original.firstName,original.lastname,original.accountBalance);
        this.hourlyRate = original.hourlyRate;
    }

    public double getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(double hourlyRate) {
        this.hourlyRate = hourlyRate;
    }


    @Override
    public String toString() {
        return "Student{" +
                "uuid=" + uuid +
                ", firstName='" + firstName + '\'' +
                ", lastname='" + lastname + '\'' +
                ", accountBalance=" + accountBalance +
                ", hourlyRate=" + hourlyRate +
                ", getHourlyRateThreshold=" + getHourlyRateThreshold +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return  Double.compare(student.getHourlyRate(), getHourlyRate()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getHourlyRate());
    }



    @Override
    public boolean applyTax() {
        double calculatedTax=0;
        if(hourlyRate > getHourlyRateThreshold){
            calculatedTax = accountBalance*GeneralTaxingConstants.TAX_STUDENT;
            accountBalance = accountBalance - calculatedTax;
            return true;
        }else {
            return false;
        }

    }

    @Override
    public double getCalculatedTax() {
        double calculatedTax;
        if(hourlyRate>getHourlyRateThreshold){
            calculatedTax = accountBalance * GeneralTaxingConstants.TAX_STUDENT;
            return calculatedTax;
        }else {
            return  calculatedTax = 0;
        }

    }
}
