package Models;

import java.util.List;
import java.util.UUID;

public interface WorkerRepository {
    Worker get(UUID id);
    void add(Worker worker);
    boolean update(Worker worker);
    void remove(Worker worker);
    List<Worker> getAllWorkers();
}
